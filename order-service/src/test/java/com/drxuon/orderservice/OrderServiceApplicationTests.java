package com.drxuon.orderservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;

@SpringBootTest
class OrderServiceApplicationTests {
	@Container
	private static final MySQLContainer MY_SQL_CONTAINER = new MySQLContainer();

	@Test
	void contextLoads() {
	}

}
