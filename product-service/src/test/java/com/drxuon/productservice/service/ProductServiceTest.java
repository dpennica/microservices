package com.drxuon.productservice.service;

import com.drxuon.productservice.dto.ProductRequest;
import com.drxuon.productservice.repository.ProductRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@Testcontainers
@TestMethodOrder(MethodOrderer.MethodName.class)
class ProductServiceTest {
    @Container
    static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:6.0.3");
    @Autowired
    private ProductRepository productRepository;
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Autowired
    private ProductService productService;
    @Test
    void createProduct() {

        productService.createProduct(getProductRequest());

        Assertions.assertEquals(1, productRepository.findAll().size());

    }

    private ProductRequest getProductRequest() {
        return ProductRequest.builder()
                .name("Ferrari Dino")
                .description("Ferrari dedicated to Dino Ferrari")
                .price(BigDecimal.valueOf(500000))
                .build();
    }

    @Test
    void getAllProducts() {
        productService.getAllProducts();
        Assertions.assertEquals(1, productService.getAllProducts().size());
    }
}